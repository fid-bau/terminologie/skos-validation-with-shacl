# SKOS Validation with SHACL

## Description
SHACL Shapes for SKOS validation

## Related resources

- <https://github.com/cmader/qSKOS/tree/f33824286594e8f10e54bf4ab54d6faded7c3151/src/main/java/at/ac/univie/mminf/qskos4j/issues>
- <https://op.europa.eu/documents/3938058/5352035/skosShapes.shapes.ttl/6f355b14-6b3e-844a-e05d-1528dd40c4b5?t=1545061378000>

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

## Project status
Draft